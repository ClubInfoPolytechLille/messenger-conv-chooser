# Issues

## Major Issue
Messenger Changed its DOM layout. Thus the algorithm is still good, but implementation will have to be changed.

## Minor Issues

### issue0 before DOM change
When the page you are on is not a "wanted conversation", the conversation content will stay displayed while the conversation's `<li>` won't be displayed
* solution idea: create a redirection
* solution idea: not usefull enough to waste time on it

### issue1 before DOM change
When you receive a message from an "unwanted conversation", a message "impossible to display conversation list" will replace the conversation list.
* solution idea: interceipt the xhr response and destroy it before the facebook's function get it.