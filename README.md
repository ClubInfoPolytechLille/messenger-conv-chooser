# Messenger Conv Chooser

As you may have multiple profiles for multiple topics in your browser, you may want not to see some messenger conversations not related to the topic of the browser profile. The source code you will find in this repository, used with the resource override extension, will let you do this.