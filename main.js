
console .log("MessengerConvChooser seems working");
var l="https://www.messenger.com/t/".length;
var wantedConversations=["example"];
var ul;
var t=[];

/*drag out a conversation from the list,
the Id of the conversation will be registered in the t array,
the t array will be printed in the console after any addition to the array
*/
function getIdConv(){
    ul.addEventListener("dragstart", function( event ) {
        
      dragged = event.target;
      
      t.push(dragged.href.substring(l));
      console.log(t);
      for(var test of t){
        if(t.includes(test)){
            //console.log ('ok');
        }
        else{alert("MessengerConvChooser : fatal error");}
      }
    }, false);
}

function clearUnwantedConversations(){
    
    for(var li of ul.querySelectorAll("li")){
        
            //console.log(li);
        var a=li.querySelector("a");
            //console.log(a);
        var idConv=a.href;
            //console.log(idConv);
        if(idConv===undefined || idConv===""){// means that we are refering to a group conversation
            idConv=a.dataset.href;
            //console.log(idConv);
        }
        
        idConv=idConv.substring(l);//gets the last part of the URL
            //console.log(idConv);
        if(!wantedConversations.includes(idConv)){
            //console.log(idConv);
            li.remove();
            return;
        }
    }
}

window.onload = (event) => {
    /*Configuration d'un mutation observer pour detecter des changements dans la <ul> listes des conversations,
    i.e. un nouveau message arrive et l'ordre des conversations change, ou simplement s'ajoute aux conversations
    */
    ul= document.querySelectorAll("div[aria-label=Conversations] ul")[0];
    const config = {childList: true };
    const callback = function(mutationsList, observer) {
    
        for(const mutation of mutationsList) {
            if (mutation.type === 'childList') {
                clearUnwantedConversations();
            }
        }
    };
    const observer = new MutationObserver(callback);
    observer.observe(ul, config);


    //console.log("wc : "+(wantedConversations.length))
    if(!wantedConversations.length){
        //console.log("wanted conv empty")
        getIdConv();
    }
    else {
        //console.log(wantedConversations);
        clearUnwantedConversations();
        
    }
    console.log('page is fully loaded');
};

